using GitLabCodecov: f, g
using Test

@testset "GitLabCodecov.jl" begin
    if VERSION.minor == 0
        f(true)
        g(false)
    else
        f(false)
        g(true)
    end
end
