# GitLabCodecov

[![Build Status](https://gitlab.com/christopher-dG/GitLabCodecov.jl/badges/master/pipeline.svg)](https://gitlab.com/christopher-dG/GitLabCodecov.jl/pipelines)
[![Coverage](https://gitlab.com/christopher-dG/GitLabCodecov.jl/badges/master/coverage.svg)](https://gitlab.com/christopher-dG/GitLabCodecov.jl/commits/master)
[![Coverage](https://codecov.io/gh/christopher-dG/GitLabCodecov.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/christopher-dG/GitLabCodecov.jl)
