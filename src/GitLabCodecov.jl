module GitLabCodecov

function f(x)
    if x
        @show x
    else
        @show !x
    end
end

function g(x)
    if x
        @show x
    else
        @show !x
        println(x)
    end
end

end
